Get SSH public keys via Github

Pulls the ssh pubkeys from Github of the specified username. Then installs
those for the specified user.

```
ansible-playbook ssh_keys.yaml -i <hostname>, --extra-vars "ansible_user=<username> github_user=<github_username>"
```
